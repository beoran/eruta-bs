#ifndef zori_button_H_INCLUDED
#define zori_button_H_INCLUDED


#include "zori.h"
#include "zori_caption.h"


#define ZORI_WIDGET_TYPE_BUTTON ZORI_WIDGET_TYPE('z','b','u','t')

struct zori_button {
    struct zori_widget  widget;
    struct zori_caption caption;
    int                 align;
};


struct zori_button *zori_widget_to_button(struct zori_widget *widget);
int zori_button_on_mouse_axes(union zori_event *event);
int zori_button_on_mouse_click(union zori_event *event);
void zori_draw_button(struct zori_button *button);
int zori_button_on_draw(union zori_event *event);
struct zori_button *zori_button_init(struct zori_button *button, const char *text);
struct zori_button *zori_button_new(zori_id id, zori_id parent, zori_box *box, const char *text);
zori_id zori_new_button(zori_id id, zori_id parent, zori_box *box, const char * text);


#endif




