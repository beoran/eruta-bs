#ifndef zori_page_H_INCLUDED
#define zori_page_H_INCLUDED

#include "zori.h"

#define ZORI_WIDGET_TYPE_PAGE ZORI_WIDGET_TYPE('z','p','a','g')

/* In Zori, the GUI is paginated. This means that on any 
 * screen, only a single GUI page can be active. The intent is to 
 * support different GUI modes such as startup screen, status view, 
 * settings, HUD, and so on between which can be switched easily. */
struct zori_page { 
  /* A page is a widget. */ 
  struct zori_widget widget;
};


#include "zori_page_proto.h"

#endif




