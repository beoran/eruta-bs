#ifndef zori_caption_H_INCLUDED
#define zori_caption_H_INCLUDED


/* Caption helper struct for use by wigdets that have captions. */
struct zori_caption {
  zori_string       * text;
  struct zori_style * style;
  zori_box            box;
};


struct zori_caption *
zori_caption_set(struct zori_caption * caption, const zori_string * text);

struct zori_caption *
zori_caption_set_cstr(struct zori_caption * caption, const char * cstr);

struct zori_caption *zori_caption_init(struct zori_caption *caption, const char *cstr);
void zori_caption_draw(const struct zori_caption *caption, const zori_rebox *box, const struct zori_style *style);
void zori_caption_done(struct zori_caption *caption);


#endif

