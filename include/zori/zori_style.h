#ifndef zori_style_H_INCLUDED
#define zori_style_H_INCLUDED

struct zori_style *zori_get_default_style(void);
void zori_destroy_default_style(void);
zori_id zori_initialize_default_style(void);
struct zori_style *zori_get_default_style(void);
void zori_destroy_default_style(void);

#endif

