/* This file was generated by runcprotoall */

#ifndef CPROTO /* Needed to protect cproto from itself. */
#ifndef zori_longtext_proto_included
/* src/zori/zori_longtext.c */
struct zori_longtext *zori_widget_to_longtext(struct zori_widget *widget);
struct zori_longtext *zori_longtext_set(struct zori_longtext *longtext, const zori_string *text);
struct zori_longtext *zori_longtext_set_cstr(struct zori_longtext *longtext, const char *cstr);
struct zori_longtext *zori_longtext_init_cstr(struct zori_longtext *longtext, const char *cstr);
void zori_longtext_done(struct zori_longtext *longtext);
int zori_longtext_page(struct zori_longtext *longtext);
int zori_longtext_last_page(struct zori_longtext *longtext);
int zori_longtext_page_(struct zori_longtext *longtext, int page);
int zori_longtext_at_end(struct zori_longtext *longtext);
void zori_longtext_update_longtext(struct zori_longtext *longtext, double dt);
int zori_longtext_longtext_at_end(struct zori_longtext *longtext);
void zori_longtext_draw_text(struct zori_longtext *longtext);
void zori_longtext_draw_partial_text(struct zori_longtext *longtext);
void zori_draw_longtext(struct zori_longtext *longtext);
zori_id zori_set_line_stop(zori_id index, int stop);
zori_id zori_line_start_(zori_id index, int start);
int zori_delay_(zori_id index, double delay);
int zori_line_stop(int index);
int zori_line_start(int index);
double zori_delay(int index);
int zori_page_lines_(zori_id index, int lines);
int zori_page_lines(int index);
int zori_paused_(zori_id index, int paused);
int zori_paused(zori_id index);
int zori_page(zori_id index);
int zori_last_page(zori_id index);
int zori_at_end(zori_id index);
int zori_page_(zori_id index, int page);
int zori_next_page(zori_id index);
int zori_previous_page(int index);
int zori_longtext_on_mouse_axes(union zori_event *event);
int zori_longtext_on_mouse_click(union zori_event *event);
int zori_longtext_on_draw(union zori_event *event);
struct zori_longtext *zori_longtext_init(struct zori_longtext *longtext, const char *text);
struct zori_longtext *zori_longtext_new(zori_id id, zori_id parent_id, zori_box *box, const char *text);
zori_id zori_new_longtext(zori_id id, zori_id parent, zori_box *box, const char *text);

#endif /* zori_longtext_proto_included */ 
#endif /* CPROTO */