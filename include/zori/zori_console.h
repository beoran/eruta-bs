#ifndef zori_console_H_INCLUDED
#define zori_console_H_INCLUDED

#define ZORI_WIDGET_TYPE_CONSOLE ZORI_WIDGET_TYPE('z','c','o','n')

typedef int (zori_console_command)(struct zori_widget *, const char * command, void * data);

/* A console is a console for command-line interaction and error display. When it's active it captures all input (as long as it's active) */
struct zori_console {
  struct zori_widget widget;
  USTRList  text;
  int       count;
  int       max;
  int       start;
  int       charw;
  int       cursor;
  char    * buf;
  USTR    * input;
  zori_console_command * command; // called when a command has been entered, if set.
  void    * command_data; // command data.
};



struct zori_console *zori_widget_console(struct zori_widget *widget);
int zori_console_docommand(struct zori_console *self, const char *text);
int zori_console_addstr(struct zori_console *self, const char *str);
int zori_console_addustr(struct zori_console *self, const ALLEGRO_USTR *ustr);
int zori_console_puts(struct zori_console *self, const char *str);
int zori_console_vprintf(struct zori_console *self, const char *format, va_list args);
int zori_console_printf(struct zori_console *self, const char *format, ...);
int zori_console_draw(union zori_event *zevent);
void zori_console_active_(struct zori_console *self, int active);
int zori_console_active(struct zori_console *self);
int zori_console_scroll(struct zori_console *self, int direction);
int zori_console_handle_keychar(union zori_event *zevent);
int zori_console_handle_keydown(union zori_event *zevent);
int zori_console_handle_mouseaxes(union zori_event *zevent);
int zori_console_handle_ignore(union zori_event *zevent);
int zori_console_handle(struct zori_widget *widget, zori_system_event *sevent);
int zori_console_done(struct zori_widget *widget);
int zori_console_free(struct zori_widget *widget);
struct zori_console *zori_console_alloc(void);
struct zori_console *zori_console_initall(struct zori_console *self, int id, zori_rebox *bounds, struct zori_style *style);
struct zori_console *zori_console_new(int id, zori_rebox *bounds, struct zori_style *style);



#endif




