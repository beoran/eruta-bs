#ifndef zori_screen_H_INCLUDED
#define zori_screen_H_INCLUDED

#include "zori.h"

enum {
   ZORI_WIDGET_TYPE_SCREEN = ZORI_WIDGET_TYPE('z','s','c','r')
};

/* The top level widget for a single display. */
struct zori_screen { 
  /* A screen is a widget. */ 
  struct zori_widget widget;
  /* It also manages the cursors. */
  struct zori_cursors cursors;
  /* Display this screen is on. */
  zori_display * display; 
  /* The GUI page that is active on this screen if any. */
  struct zori_page * active_page; 
};


#include "zori_screen_proto.h"

#endif




