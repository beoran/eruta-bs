#ifndef zori_longtext_H_INCLUDED
#define zori_longtext_H_INCLUDED

#include "zori.h"

#define ZORI_WIDGET_TYPE_LONGTEXT ZORI_WIDGET_TYPE('z','l','t','x')

/* A descriptor for the current state of the longtext */
struct zori_longtext_state {
  /*- Time waited between display of characters */
  double wait;
  
  /*- Virtual text page we are currently on. */
  int page;
  
  /*- Line of text we are currently on. */
  int line;
  
  /*- Currently paused or not. */
  int paused;
  
  /*- Total amount of lines to display for this text. */
  int total;
  
  /*- Current position in current line. */
  int position;
  
  /*- Animation timer. */
  double anitime;
  
}; 


/** Flags for the settings of a longtext widget.*/
enum zori_longtext_setting_flag {
    /*- Disable animation of pause marker. */
    ZORI_LONGTEXT_SETTING_FLAG_STATIC_MARKER  = 1 << 0,
    /*- Show letters oneby one in stead of space separated words.  */
    ZORI_LONGTEXT_SETTING_FLAG_SHOW_LETTERS  = 1 << 1,
};

/* A descriptor for the configurable size settings of a 
 * longtext. */
struct zori_longtext_settings {
  /*- Amount of lines to display in one virtual page of text. */
  int lines;
  /*- Vertcal spacing between lines.  */
  int spacing;
  /*- Delay between display of the individual characters. */
  double delay;  
  /*- Several flags that modify the behavior of the box. */
  enum zori_longtext_setting_flag flags;
};

struct zori_longtext {
  struct zori_widget widget;
  int                align;
  zori_string      * caption;
  zori_string      * text;
  
  /*- Configurable settings. */
  struct zori_longtext_settings settings;  
  /*- Current state. */
  struct zori_longtext_state    state;
};
 
#include "zori_longtext_proto.h"

#endif

