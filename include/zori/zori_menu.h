#ifndef zori_menu_H_INCLUDED
#define zori_menu_H_INCLUDED

#define ZORI_WIDGET_TYPE_MENU ZORI_WIDGET_TYPE('z','m','e','n')

struct zori_menu {
  struct zori_widget widget;
  /* The index of the menu item that currently is selected. Negative means none. */
  int selected_index;
};

#include "zori_menu_proto.h"

#endif




