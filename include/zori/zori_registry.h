#ifndef zori_registry_H_INCLUDED
#define zori_registry_H_INCLUDED


int zori_registry_entry_compare(const void *v1, const void *v2);
zori_id zori_registry_init(struct zori_registry *registry);
void zori_registry_destroy(struct zori_registry *registry);
zori_id zori_registry_add(struct zori_registry *registry, zori_id id, struct zori_widget *widget);
struct zori_registry_entry *zori_registry_lookup_entry(struct zori_registry *registry, zori_id id);
struct zori_widget *zori_registry_lookup(struct zori_registry *registry, zori_id id);
zori_id zori_registry_remove(struct zori_registry *registry, zori_id id);
zori_id zori_initialize_registry(void);
zori_id zori_destroy_registry(void);
struct zori_registry *zori_get_registry(void);

#endif
