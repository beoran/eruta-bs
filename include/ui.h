#ifndef ui_H_INCLUDED
#define ui_H_INCLUDED



/* Data struct for the particular GUI state */
struct ui_state {
  zori_id screen;
  zori_id console;
  
  int mouse_image;
  int keyjoy_image;
  
  /* main page/menu */
  struct ui_state_main {
    zori_id page;
    zori_id menu;
    
    int background_image;
    
    /* menu buttons */
    struct ui_state_main_buttons { 
      zori_id resume;
      zori_id new;
      zori_id settings;      
    } button;
  } main;
  
  /* HUD page and widgets */
  struct ui_state_hud {
    int background_image;
    zori_id page;
    zori_id dialog;
  } hud;
  
  /* Settings page and widgets */
  struct ui_state_settings {
    zori_id page;    
    zori_id menu;    
  } settings;
  
};


void ui_setup(void);
void ui_state_init(struct ui_state * ui, zori_display * display, zori_font * font);

#endif




