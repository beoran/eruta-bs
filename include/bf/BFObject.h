#ifndef BF_OBJECT_H_INCLUDED
#define BF_OBJECT_H_INCLUDED

typedef void * BFID; 

#define BF_JOIN_AID(A, B)       A##B
#define BF_JOIN(A, B)           BF_JOIN_AID(A, B)
#define BF_JOIN3(A, B, C)       BF_JOIN(BF_JOIN(A, B), C)
#define BF_JOIN4(A, B, C, D)    BF_JOIN(BF_JOIN3(A, B, C), D)

#define bf_message(RESULT, NAME, ...)                                 \
extern int BF_JOIN(bf_message_, NAME);                                \
extern RESULT BF_JOIN(bf_method_, NAME) (BFID self, __VA_ARGS__)


#define bf_method(RESULT, SELFTYPE, NAME, ...)                                  \
int BF_JOIN4(bf_message_, SELFTYPE, _, NAME);                                   \
RESULT BF_JOIN(bf_method_, NAME) (BFID selfid, __VA_ARGS__)  {                  \
  SELFTYPE * self = (SELFTYPE *) selfid;
  
#define bf_endmethod }

  

typedef struct Ohohoho_ {  
  int frotz;
} Ohoho;


bf_message(BFID, frotz_, int value);

bf_method(BFID, Ohoho, frotz_, int value)
  self->frotz = value;
  return self;
bf_endmethod


#endif

