/* This file was generated by runcprotoall */

#ifndef CPROTO /* Needed to protect cproto from itself. */
#ifndef store_proto_included
/* src/store.c */
_Bool store_init(void);
_Bool store_index_ok(int index);
int store_max(void);
Resor *store_get(int index);
_Bool store_drop(int index);
Resor *store_put(int index, Resor *value);
_Bool store_done(void);
Resor *store_load_ttf_font_stretch(int index, const char *vpath, int w, int h, int f);
Resor *store_load_ttf_font(int index, const char *vpath, int h, int f);
Resor *store_load_bitmap_font_flags(int index, const char *vpath, int f);
Resor *store_load_bitmap_font(int index, const char *vpath);
Resor *store_grab_font_from_resor(int index, Resor *resor, int count, int ranges[]);
Resor *store_grab_font(int index, int bmp_index, int count, int ranges[]);
Resor *store_load_audio_stream(int index, const char *vpath, size_t buffer_count, int samples);
Resor *store_load_sample(int index, const char *vpath);
Resor *store_load_bitmap_flags(int index, const char *vpath, int flags);
Resor *store_load_bitmap(int index, const char *vpath);
Resor *store_load_other(int index, const char *vpath, ResorKind kind, ResorLoader *loader, ResorDestructor *destroy, void *extra);
Resor *store_load_tilemap(int index, const char *vpath);
int store_kind(int index);
ALLEGRO_FONT *store_get_font(int index);
ALLEGRO_BITMAP *store_get_bitmap(int index);
ALLEGRO_SAMPLE *store_get_sample(int index);
ALLEGRO_AUDIO_STREAM *store_get_audio_stream(int index);
void *store_get_other(int index, unsigned kind);
_Bool store_get_bitmap_format(int index, int *value);
_Bool store_get_bitmap_flags(int index, int *value);
_Bool store_get_bitmap_height(int index, int *value);
_Bool store_get_bitmap_width(int index, int *value);
_Bool store_get_ustr_dimensions(int index, ALLEGRO_USTR *text, Rebox *value);
_Bool store_get_ustr_width(int index, ALLEGRO_USTR *text, int *value);
_Bool store_get_text_dimensions(int index, char *text, Rebox *value);
_Bool store_get_text_width(int index, char *text, int *value);
_Bool store_get_font_ascent(int index, int *value);
_Bool store_get_font_descent(int index, int *value);
_Bool store_get_font_line_height(int index, int *value);
int store_get_unused_id(int minimum);
int store_load_ttf_font_stretch_id(int min, const char *vpath, int w, int h, int f);
int store_load_ttf_font_id(int min, const char *vpath, int h, int f);
int store_load_bitmap_font_flags_id(int min, const char *vpath, int f);
int store_load_bitmap_font_id(int min, const char *vpath);
int store_grab_font_id(int min, int from, int count, int ranges[]);
int store_load_audio_stream_id(int min, const char *vpath, size_t buffer_count, int samples);
int store_load_sample_id(int min, const char *vpath);
int store_load_bitmap_flags_id(int min, const char *vpath, int flags);
int store_load_bitmap_id(int min, const char *vpath);
int store_load_other_id(int min, const char *vpath, ResorKind kind, ResorLoader *loader, ResorDestructor *destroy, void *extra);
int store_load_tilemap_id(int min, const char *vpath);

#endif /* store_proto_included */ 
#endif /* CPROTO */