#include <allegro5/allegro_color.h>


#include "zori.h"
#include "zori_style.h"

static struct zori_style * the_default_style = NULL;

/** Initialize the global default style. */
zori_id zori_initialize_default_style(void) {
  the_default_style = calloc(1, sizeof(*the_default_style));
  if (!the_default_style) return ZORI_ID_ENOMEM;
 
  the_default_style->text.font      = al_create_builtin_font();  
  the_default_style->text.color     = al_color_name("white");
  the_default_style->text.flags     = ALLEGRO_ALIGN_LEFT;
  the_default_style->border.color   = al_color_name("white");  
  the_default_style->back.color     = al_color_name("green");
    
  return ZORI_ID_OK;
}


/** Returns the global default style */
struct zori_style * zori_get_default_style() {
  return the_default_style;
}

/** Destroys the global default style. */
void zori_destroy_default_style(void) {
  free(the_default_style);
  the_default_style = NULL;
} 


/* Fills in a background style part. */
struct zori_background_style *  zori_background_style_init(
struct zori_background_style * bs, zori_color color, zori_bitmap * bitmap, int corner_radius, int flags) {
  if (!bs) return NULL;  
  bs->color         = color;
  bs->image         = bitmap;
  bs->flags         = flags;
  bs->radius        = corner_radius;
  return bs;
};

/* Fills in a text style part. */
struct zori_text_style *  zori_text_style_init(struct zori_text_style * ts, 
zori_color color, zori_font * font, int flags) {
  if (!ts) return NULL;
  ts->color      = color;
  ts->font       = font;
  ts->flags      = flags;
  return ts;
};

 
