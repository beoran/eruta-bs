
#include "zori.h"
#include "zori_caption.h"
#include "monolog.h"


struct zori_caption *
zori_caption_set(struct zori_caption * caption, const zori_string * text) {
  if (caption) {
    if (caption->text) {
      ustr_free(caption->text);
      caption->text = NULL;
    }
    if (text) { 
      caption->text = ustr_dup(text);
      if (!caption->text) {
        LOG_ERROR("Out of memory in caption setup.");
      }
    }
  }
  return caption;
} 

struct zori_caption *
zori_caption_set_cstr(struct zori_caption * caption, const char * cstr) {
  const USTR * ustr; 
  USTR_INFO info;
  ustr = ustr_refcstr(&info, cstr);
  return zori_caption_set(caption, ustr);
}

struct zori_caption *
zori_caption_init(struct zori_caption * caption, const char * cstr) {
  caption->text = NULL;
  return zori_caption_set_cstr(caption, cstr);
}

void 
zori_caption_draw(
  const struct zori_caption * caption, 
  const zori_rebox * box, 
  const struct zori_style * style) {
 
  if (caption->text) {
    zori_font * font = style->text.font;
    zori_color color = style->text.color;
    float x = box->at.x;
    float y = box->at.y;
    float w = box->size.x;
    if (style->text.flags & ZORI_FONT_ALIGN_CENTRE) {
      x = x + (w / 2);
    }
    
    if (style->text.flags & ZORI_FONT_ALIGN_RIGHT) {
      x = x + w;
    }
    
    al_draw_multiline_ustr(font, color, x, y, w, -1,  style->text.flags, 
                           caption->text);
  }
}

void zori_caption_done(struct zori_caption * caption) {
  zori_caption_set(caption, NULL);
}







