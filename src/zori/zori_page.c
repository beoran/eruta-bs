#include "zori.h"
#include "zori_widget.h"
#include "zori_caption.h"
#include "zori_page.h"

#include "monolog.h"

/* Magic comment for runcprotoall: @generate_cproto@ */


struct zori_page * zori_widget_to_page(struct zori_widget * widget) {
  if (!zori_widget_is_type(widget, ZORI_WIDGET_TYPE_PAGE)) return NULL;
  return ZORI_CONTAINER_OF(widget, struct zori_page, widget);
}

struct zori_page * zori_id_to_page(zori_id id) {
  struct zori_widget * widget = zori_get_widget(id);
  if (!widget) return NULL;
  return zori_widget_to_page(widget);
}

int zori_page_on_draw(union zori_event * event) {
  struct zori_widget * widget = event->any.widget;
  struct zori_page   * page   = zori_widget_to_page(widget);
  struct zori_style  * style  = &widget->style;
  if (style->back.image) {
    al_draw_bitmap(style->back.image, 0, 0, 0);
  } 
  
  if (style->back.flags & ZORI_STYLE_FLAG_FILL) {
    al_clear_to_color(style->back.color);
  }
  
  return ZORI_HANDLE_IGNORE;
}


struct zori_handler zori_page_handlers[] = {
  { ZORI_EVENT_DRAW                     , zori_page_on_draw          , NULL },
  { -1, NULL, NULL }
};


struct zori_page * zori_page_new(zori_id id, struct zori_widget * parent) {
  struct zori_page * page = NULL;
  if (!parent) return NULL;
  page = calloc(1, sizeof(*page));
  if (!page) return NULL;
  zori_widget_initall(&page->widget, ZORI_WIDGET_TYPE_PAGE, id, parent, 
                      NULL, NULL, ZORI_ARRAY_AND_AMOUNT(zori_page_handlers)); 
  return page;
}


zori_id zori_new_page(zori_id id, zori_id parent_id) {
  struct zori_widget * parent = zori_get_widget(parent_id);
  struct zori_page   * page   = zori_page_new(id, parent);
  if (!page) return ZORI_ID_ENOMEM;
  return page->widget.id;
}


