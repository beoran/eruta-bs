#include "zori.h"
#include "zori_widget.h"
#include "zori_registry.h"
#include "zori_style.h"
#include "zori_screen.h"
#include "monolog.h"
#include "draw.h"

/* Magic comment for runcprotoall: @generate_cproto@ */


bool zori_widget_is_type(struct zori_widget * widget, zori_widget_type type) {
  if(!widget) return false;
  return (type) == widget->type; 
}

bool zori_widget_is_type_predicate(struct zori_widget * widget, void * extra) {
  zori_widget_type * type_ptr = extra;
  if(!type_ptr) return false;
  return zori_widget_is_type(widget, (*type_ptr)); 
}

struct zori_widget * 
  zori_widget_get_parent_of_type(struct zori_widget * widget,  
  zori_widget_type type) {
  return zori_widget_find_parent(widget, zori_widget_is_type_predicate, &type);
}


struct zori_screen * zori_widget_get_screen(struct zori_widget * widget) {  
  struct zori_widget * screen_widget = zori_widget_get_parent_of_type(widget, ZORI_WIDGET_TYPE_SCREEN);
  return zori_widget_to_screen(screen_widget);
}


/** Returns whether or not a widget will even handle an event in the first place.
 * For example, a hidden widget won't draw, and a disabled widget won't 
 * accept any system events, and a NULL widget doesn't accept events at all.. */
int zori_widget_accepts_event(struct zori_widget * widget, union zori_event * event) {
  if (!widget) return 0;
  switch(event->any.type) {
    case ZORI_EVENT_DRAW:
      return zori_widget_visible(widget);
    default:
      return zori_widget_active(widget);
  }
}   


/** Raises an event on the widget itself only. */
int zori_widget_self_raise_event(struct zori_widget * widget, 
union zori_event * event) {
  enum zori_handle_result result;
  if (zori_widget_accepts_event(widget, event)) { 
    event->any.widget = widget;
    return zori_handlers_handle(&widget->handlers, event);
  }
  /* If the event is not accepted, it is NOT passed on to the child widgets. */
  return ZORI_HANDLE_DONE;
}

/* Raises an event on the widget, and if necessary, propagates it on to it's 
 * children automatically and recursively. */
int 
zori_widget_raise_event(struct zori_widget * widget, union zori_event * event) {
  if (!widget) return ZORI_HANDLE_DONE;
  
  enum zori_handle_result result = 
  zori_widget_self_raise_event(widget, event);
  
  if (zori_propagate_event_p(result)) {
    enum zori_handle_result sub;
    size_t index;
    for (index = 0; index < miao_size(&widget->children); index++) {
      struct zori_widget * child = miao_unsafe_get(&widget->children, index);
      event->any.widget = child;
      sub = zori_widget_raise_event(child, event);
      if (sub == ZORI_HANDLE_ERROR) {
        result = ZORI_HANDLE_ERROR;
        break;
      }
    }
  }
  return result;
}

int zori_widget_raise_system_event
(struct zori_widget * widget, zori_system_event * sysev) {
  union zori_event event;
  event.sys.any.type   = sysev->type;
  event.sys.any.widget = widget;
  event.sys.ev         = sysev;
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_draw_event(struct zori_widget * widget) {
  union zori_event event;
  event.draw.any.type   = ZORI_EVENT_DRAW;
  event.draw.any.widget = widget; 
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_overdraw_event(struct zori_widget * widget) {
  union zori_event event;
  event.draw.any.type   = ZORI_EVENT_OVERDRAW;
  event.draw.any.widget = widget; 
  return zori_widget_raise_event(widget, &event);
}


int zori_widget_raise_done_event(struct zori_widget * widget) {
  union zori_event event;
  event.done.any.type   = ZORI_EVENT_DONE;
  event.done.any.widget = widget; 
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_free_event(struct zori_widget * widget) {
  union zori_event event;
  event.free.any.type   = ZORI_EVENT_FREE;
  event.free.any.widget = widget; 
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_update_event
(struct zori_widget * widget, double dt) {
  union zori_event event;
  event.update.any.type   = ZORI_EVENT_UPDATE;
  event.update.any.widget = widget;
  event.update.dt         = dt; 
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_action_event
(struct zori_widget * widget) {
  union zori_event event;
  event.action.any.type   = ZORI_EVENT_ACTION;
  event.action.any.widget = widget;
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_internal_action_event
(struct zori_widget * widget) {
  union zori_event event;
  event.action.any.type   = ZORI_INTERNAL_EVENT_ACTION;
  event.action.any.widget = widget;
  return zori_widget_raise_event(widget, &event);
}

int zori_widget_raise_close_event
(struct zori_widget * widget, struct zori_widget * from) {
  union zori_event event;
  event.close.any.type   = ZORI_EVENT_CLOSE;
  event.close.any.widget = widget;
  event.close.from       = from;
  return zori_widget_raise_event(widget, &event);
}


int zori_widget_compare(const void * v1, const void * v2) {
  const struct zori_widget * w1 = v1, * w2 = v2;
  return (w2->id - w1->id);
}

struct zori_handler * zori_widget_add_handler
(struct zori_widget * widget, zori_event_type type, zori_handler_func * handler, void * data) {  
  if ((!widget) || (!handler)) return NULL;
  
  return zori_handlers_add(&widget->handlers, type, handler, data);
}

struct zori_handler * zori_widget_add_handlers
(struct zori_widget * widget, struct zori_handler * handlers, size_t amount) {
  size_t i;
  for (i = 0; i < amount; i++) { 
    struct zori_handler * handler = handlers + i;
    if (!zori_widget_add_handler(widget, handler->type, handler->handler, handler->data)) return NULL;
  }
  return handlers + amount - 1;
}

/* Gets the style to use for a marked widget. Looks up a screen widget and then 
 * looks for the cursor t determine this. */
struct zori_style * zori_widget_get_mark_style(struct zori_widget * widget) {
    struct zori_screen * screen = zori_widget_get_screen(widget);
    if (!screen) return &widget->style;
    return &(screen->cursors.keyjoy.target_style);
}

/* Gets the style to use for a hovered widget. Looks up a screen widget and then 
 * looks for the cursor t determine this. */
struct zori_style * zori_widget_get_hover_style(struct zori_widget * widget) {
    struct zori_screen * screen = zori_widget_get_screen(widget);
    if (!screen) return &widget->style;
    return &(screen->cursors.mouse.target_style);
} 



void zori_widget_free(struct zori_widget * widget);

struct zori_widget * zori_widget_done(struct zori_widget * widget) {
  size_t index;
  struct zori_widget * aid, * next;
  if (!widget) return widget;
  
  for (index = 0; index < miao_size(&widget->children); index ++) {
    struct zori_widget * child = miao_unsafe_get(&widget->children, index);
    zori_widget_free(child);
  }
  
  if (widget->destroy) {
      widget->destroy(widget);
  }
      
  miao_done(&widget->handlers);
  miao_done(&widget->children);
  
  return widget;
}

void zori_widget_free(struct zori_widget * widget) {
  if (widget) {
    zori_registry_remove(zori_get_registry(), widget->id);
  }
  zori_widget_done(widget);
  free(widget);
}

struct zori_widget *  
zori_widget_add_child(struct zori_widget * parent, struct zori_widget * child) {
  if (parent) { 
    miao_push((&parent->children), child);
    child->parent = parent;
  }
  return child;
} 

zori_id
zori_widget_margins_(struct zori_widget * widget, int left, int top, int right, int bottom) {
  if (!widget) return ZORI_ID_EINVAL;
  widget->outer = widget->box;
  widget->outer.at.x -= left;
  widget->outer.at.y -= top;
  widget->outer.size.x += (left + right);
  widget->outer.size.y += (top + bottom);
  return widget->id;
}

zori_id 
zori_widget_margin_(struct zori_widget * widget, int size) {
  return zori_widget_margins_(widget, size, size, size, size);
}



/* Draws a widget's frame based on it's style. If the background bitmap is set 
 * then that is used, otherwise,  the background color is used, including a border
 * if needed. */
void zori_widget_draw_background(struct zori_widget * widget) {
    float dx, dy, dw, dh;
    struct zori_style * style = &widget->style;
    dx = rebox_x(&widget->box);
    dy = rebox_y(&widget->box);
    dw = rebox_w(&widget->box);
    dh = rebox_h(&widget->box);
    
    if (zori_widget_hover(widget)) {
      style = zori_widget_get_hover_style(widget);
    }
    
    if (zori_widget_marked(widget)) {
      style = zori_widget_get_mark_style(widget);
    }
    
    if (style->back.image) {
      zori_bitmap * background  = style->back.image;
      image_blitscale9(background, dx, dy, dw, dh, -1, -1);
    } else {
      zori_color fillco = style->back.color;
      zori_color bordco = style->border.color;
      int thick         = style->border.size;
      draw_frame(dx, dy, dw, dh, thick, bordco, fillco);
    }
}

enum zori_handle_result 
zori_widget_must_draw_children(struct zori_widget * widget) {
  
  
  float dx = rebox_x(&widget->box);
  float dy = rebox_y(&widget->box);
  
  if (zori_widget_visible(widget)) {
    draw_roundbox(dx,  dy, 10, 10, 3, 3, al_map_rgb(64,16,255), 3);
    return ZORI_HANDLE_PASS;
  }
  draw_roundbox(dx, dy, 10, 10, 3, 3, al_map_rgb(255,16,64), 3);
  return ZORI_HANDLE_DONE;
}


int zori_widget_visible(struct zori_widget * widget)  {
  return widget && ((widget->flags & ZORI_FLAG_HIDDEN) != ZORI_FLAG_HIDDEN);
}

int zori_widget_active(struct zori_widget * widget)  {
  return widget && ((widget->flags & ZORI_FLAG_DISABLED) != ZORI_FLAG_DISABLED);
}

int zori_widget_active_(struct zori_widget * widget, int set)  {
  if (set) {
    widget->flags = widget->flags & (~ZORI_FLAG_DISABLED);
  } else {
    widget->flags = widget->flags | ZORI_FLAG_DISABLED;
  }
  return zori_widget_active(widget);
}

int zori_widget_visible_(struct zori_widget * widget, int set)  {
  if (set) {
    widget->flags = widget->flags & (~ZORI_FLAG_HIDDEN);
  } else {
    widget->flags = widget->flags | ZORI_FLAG_HIDDEN;
  }
  return zori_widget_active(widget);
}

/** Get active and visible */
int zori_widget_live(struct zori_widget * widget) {
  return zori_widget_active(widget) && zori_widget_visible(widget);
} 

/** Set active and visible */
int zori_widget_live_(struct zori_widget * widget, int set) {
  zori_widget_active_(widget, set);
  zori_widget_visible_(widget, set);
  return set;
} 




int zori_widget_hover(struct zori_widget * widget)  {
  return widget && ((widget->flags & ZORI_FLAG_HOVERED) == ZORI_FLAG_HOVERED);
}

int zori_widget_hover_(struct zori_widget * widget, int set)  {
  if (set) {
    widget->flags = widget->flags | ZORI_FLAG_HOVERED;
  } else {
    widget->flags = widget->flags & (~ZORI_FLAG_HOVERED);
  }
  return zori_widget_hover(widget);
}

int zori_widget_marked(struct zori_widget * widget)  {
  return widget && ((widget->flags & ZORI_FLAG_MARKED) == ZORI_FLAG_MARKED);
}

int zori_widget_marked_(struct zori_widget * widget, int set)  {
  if (set) {
    widget->flags = widget->flags | ZORI_FLAG_MARKED;
  } else {
    widget->flags = widget->flags & (~ZORI_FLAG_MARKED);
  }
  return zori_widget_marked(widget);
}

int zori_widget_ready(struct zori_widget * widget)  {
  return widget && ((widget->flags & ZORI_FLAG_READY) == ZORI_FLAG_READY);
}

int zori_widget_ready_(struct zori_widget * widget, int set)  {
  if (set) {
    widget->flags = widget->flags | ZORI_FLAG_READY;
  } else {
    widget->flags = widget->flags & (~ZORI_FLAG_READY);
  }
  return zori_widget_ready(widget);
}


int zori_xy_inside_widget_p(struct zori_widget * widget, double x, double y) {
    return (rebox_coordinates_inside_p(&widget->box, x, y));
}


/* Registers an event handler for a widget. */
zori_id zori_register(zori_id id,  zori_event_type type, zori_handler_func handler, void * extra);


zori_font * zori_widget_font(struct zori_widget * widget) {
  return widget->style.text.font;
}

zori_color zori_widget_forecolor(struct zori_widget * widget) {
  return widget->style.text.color;
}

zori_color zori_widget_backcolor(struct zori_widget * widget) {
  return widget->style.back.color;
}


int zori_widget_h(struct zori_widget * widget) {
  return widget->box.size.y;
}

int zori_widget_w(struct zori_widget * widget) {
  return widget->box.size.x;
}

int zori_widget_x(struct zori_widget * widget) {
  return widget->box.at.x;
}

int zori_widget_y(struct zori_widget * widget) {
  return widget->box.at.y;
}



/* Gets amount of child widgets of this widget, 0 if none, -1 on error.*/
int zori_widget_count_children(struct zori_widget * widget) { 
  if (!widget) return -1;
  return miao_size(&widget->children); 
}

/* Returns the index-th child of his widget, or NULL on error or if no such child exists. */
struct zori_widget * zori_widget_get_child(struct zori_widget * widget, int index) {
    int count = zori_widget_count_children(widget);
    if (count < 1) return NULL;
    if (index < 0) return NULL;
    if (index >= count) return NULL;
    return miao_unsafe_get(&widget->children, index);
}

zori_id
zori_widget_paddings_(struct zori_widget * widget, int left, int top, int right, int bottom) {
  if (!widget) return ZORI_ID_EINVAL;
  widget->inner = widget->box;
  widget->inner.at.x += left;
  widget->inner.at.y += top;
  widget->inner.size.x -= (left + right);
  widget->inner.size.y -= (top + bottom);
  return widget->id;
}

zori_id 
zori_widget_padding_(struct zori_widget * widget, int size) {
  return zori_widget_paddings_(widget, size, size, size, size);
}

zori_font * zori_widget_text_font(struct zori_widget * widget) {  
  struct zori_style * style;
  if (!widget) {
    style = zori_get_default_style();
  } else {
    style = & widget->style;
  }
  return style->text.font;
}

struct zori_widget * zori_widget_init
  ( struct zori_widget * widget, 
    zori_widget_type type,
    zori_id id,
    struct zori_widget * parent, 
    zori_rebox * box, struct zori_style * style) {
    if (!widget) return NULL;
    struct zori_style * default_style = zori_get_default_style(); 
    
    miao_init(&widget->children);
    miao_init(&widget->handlers);
    widget->destroy = NULL;
    widget->z     = 0;
    widget->flags = 0;
    widget->type  = type;
    widget->id    = ( (id < 0) ? zori_get_unused_id() : id);
    
    if (style) {
      widget->style = *style;
      if (!widget->style.text.font) {
        widget->style.text.font = default_style->text.font;
      }
    } else if (parent) {
      widget->style = parent->style;
    } else {
      widget->style = *default_style;
    }
    if (box) {
      widget->box = *box;
    } else if (parent) {
      widget->box = parent->box;
    } else {
      widget->box = rebox_make(0, 0, ZORI_WIDGET_DEFAULT_W, ZORI_WIDGET_DEFAULT_H);
    }
    
    zori_widget_margin_(widget, ZORI_MARGIN_DEFAULT);
    zori_widget_padding_(widget, ZORI_PADDING_DEFAULT);
    
    zori_widget_add_child(parent, widget);
    zori_registry_add(zori_get_registry(), widget->id, widget);
    return widget;
}

struct zori_widget * 
zori_widget_initall(struct zori_widget * widget, zori_widget_type type, int id,
struct zori_widget * parent, zori_rebox * box, struct zori_style * style, 
struct zori_handler * handlers, size_t amount) {
  if (zori_widget_init(widget, type, id, parent, box, style)) {
    zori_widget_add_handlers(widget, handlers, amount);
  }
  return widget;
}

/* Lets the widget handle an event and bubbles it on to it's children 
 * recursively depending on the need to do that. */
void zori_widget_handle_event(struct zori_widget * widget, union zori_event * event) {
  zori_widget_raise_event(widget, event);
}


/* Returns the first parent or super-parent of this widget for which 
 * predicate() returns true.
 */
struct zori_widget * 
zori_widget_find_parent(struct zori_widget * widget, 
  bool (*predicate)(struct zori_widget * parent, void * extra),
  void * extra 
  ) {
  struct zori_widget * parent;
  
  if (!widget) return NULL;
  parent = widget->parent;
  while (parent) {
    if (predicate(parent, extra)) return parent;
    if (parent == parent->parent) {
      LOG_ERROR("GUI setup not correct. Parent/child loop in widget %p.", widget);
      return NULL;
    }
    parent = parent->parent;
  }
  return NULL;
}



int zori_mark_widget(struct zori_widget * widget) {
    struct zori_screen * screen = zori_widget_get_screen(widget);
    if (screen) {
      screen->cursors.keyjoy.p = widget->box.at;
    }
    return ZORI_HANDLE_DONE;
}

zori_id zori_widget_set_int_result(struct zori_widget * widget, int value) {
  if (widget) { 
    widget->result.value.integer = value;
    widget->result.type          = ZORI_RESULT_TYPE_INTEGER;
    widget->result.ready         = widget->id;
    return widget->id;
  }
  return ZORI_ID_EINVAL;
}

zori_id zori_widget_set_string_result(struct zori_widget * widget, 
  zori_string * value) {
  if (widget) { 
    widget->result.value.string  = value;
    widget->result.type          = ZORI_RESULT_TYPE_STRING;
    widget->result.ready         = widget->id;
    return widget->id;
  }
  return ZORI_ID_EINVAL;
}

zori_id zori_widget_set_closed_result(struct zori_widget * widget, int value) {
  if (widget) { 
    widget->result.value.closed  = value;
    widget->result.type          = ZORI_RESULT_TYPE_CLOSED;
    widget->result.ready         = widget->id;
    return widget->id;
  }
  return ZORI_ID_EINVAL;
}


zori_id zori_set_mark_style(zori_id id, struct zori_style style) {
  struct zori_widget * widget = zori_get_widget(id);
  struct zori_screen * screen = zori_widget_get_screen(widget);
  if (screen) {
      screen->cursors.keyjoy.style = style;
      return id;
  } else {
    return ZORI_ID_EINVAL;
  }
}

zori_id zori_set_hover_style(zori_id id, struct zori_style style) {
  struct zori_widget * widget = zori_get_widget(id);
  struct zori_screen * screen = zori_widget_get_screen(widget);
  if (screen) {
      screen->cursors.mouse.style = style;
      return id;
  } else {
    return ZORI_ID_EINVAL;
  }
}


/* Moves the widget by the offset (dx, dy). Does not move it's children.  */
void zori_widget_move_self_by(struct zori_widget * widget, int dx, int dy) {
  int index, stop;
  
  widget->box.at.x    += dx;
  widget->box.at.y    += dy;
  widget->inner.at.x  += dx;
  widget->inner.at.y  += dy;
  widget->outer.at.x  += dx;
  widget->outer.at.y  += dy;
}

/* Moves resizes the widget by (dw, dh). Does not resize it's children.  
 * Margin and paddding are resized also by dw and dh;
 */
void zori_widget_resize_self_by(struct zori_widget * widget, int dw, int dh) {
  int index, stop;
  
  widget->box.size.x    += dw;
  widget->box.size.y    += dh;
  widget->inner.size.x  += dw;
  widget->inner.size.y  += dh;
  widget->outer.size.x  += dw;
  widget->outer.size.y  += dh;
}


/* Moves the widget by the offset (dx, dy). It's children will be moved as well recursively
 * keeping the offset */
void zori_widget_move_by(struct zori_widget * widget, int dx, int dy) {
  int index, stop;
  
  zori_widget_move_self_by(widget, dx, dy);
  
  stop = zori_widget_count_children(widget);
  for (index= 0 ; index < stop; index ++) {
    struct zori_widget * child = zori_widget_get_child(widget, index);
    zori_widget_move_by(child, dx, dy); 
  }
}

/* Moves the widget to (x, y). It's children will be moved as well recursively
 * keeping the offset */
void zori_widget_move_to(struct zori_widget * widget, int x, int y) {
  int dx = widget->box.at.x - x;
  int dy = widget->box.at.y - y;
  zori_widget_move_by(widget, dx, dy);
}


/* Moves the widget to (x, y). Doesn't move children. */
void zori_widget_move_self_to(struct zori_widget * widget, int x, int y) {
  int dx = widget->box.at.x - x;
  int dy = widget->box.at.y - y;
  zori_widget_move_self_by(widget, dx, dy);
}

/* Resizes the widget to (w, h). Doesn't resize children. Margins and padding will be 
 * resized accordingly. */
void zori_widget_resize_self_to(struct zori_widget * widget, int w, int h) {
  int dw = widget->box.size.x - w;
  int dh = widget->box.size.y - h;
  zori_widget_resize_self_by(widget, dw, dh);
}


/* Makes the widget fit itself to all direct children.
 * Does not resize nor move the children.
 */

void zori_widget_fit_to_children(struct zori_widget * widget) {
  int index, stop;
  int min_x = 640;
  int min_y = 480;
  int min_w = 0;
  int min_h = 0;
  
  
  struct zori_screen * screen = zori_widget_get_screen(widget);
  if (screen) {
    min_x = screen->widget.box.size.x;
    min_y = screen->widget.box.size.y;    
  } 
  
  stop = zori_widget_count_children(widget);
  for (index= 0 ; index < stop; index ++) {
    struct zori_widget * child = zori_widget_get_child(widget, index);
    if (child->outer.at.x < min_x) {
      min_x = child->outer.at.x;
    }
    if (child->outer.at.y < min_y) {
      min_y = child->outer.at.y;
    }
    if ((child->outer.size.x) > min_w) {
      min_w = child->outer.size.x;
    }
    if ((child->outer.size.y) > min_h) {
      min_h = child->outer.size.y;
    }
  }
  
  zori_widget_move_self_to(widget, min_x, min_y);
  zori_widget_resize_self_to(widget, min_w, min_h);
}

