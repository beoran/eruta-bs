
#include <bf/BFObject.h>



/* Beoran's Objects, Objective-C/Smalltalk/Javascript-ish */
struct BObject_;
typedef struct BFObject_ BFObject;

/* A Class is simply an object too. */
typedef struct BFObject_ BFClass;


typedef struct BMethod_ BFMethod;
typedef struct BFMethodTable_ BFMethodTable;
typedef struct BFInstanceVariable_ BFInstanceVariable;
typedef struct BFInstanceVariableTable_ BFInstanceVariableTable;


typedef BFObject * BMethodFunction(BFObject * self, int argc, BFObject * argv[]);

union BFValue_ {
  BFObject * bobject;
  int       integer;
  double    number;
  void    * pointer;
  char    * cstr;
};


struct BFMethod_ {
  char            * name;
  BMethodFunction * action; 
};

struct BFInstanceVariable_ {
  char            * name;
  void            * value; 
};

struct BFMethodTable_ {
  BFMethod * last;
  BFMethod * methods;
  int size;
};

struct BFInstanceVariableTable_ {
  BFInstanceVariable * last;
  BFMethodTable      * variables;
  int size;
};



struct BFObject_ {
  BFMethodTable  methods_;
  BFObject     * prototype_;
};



BFObject * bfobject_init(BFObject * self) {
  return self;
}


