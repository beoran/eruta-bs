

#import <bn/BNArray.h>
#include <stdlib.h>

@implementation BNArray
  id * data_;
  int  size_;
  
  
  - init_size: (int) amount {
    size_ = amount;
    data_ = calloc(amount, sizeof(id)); 
    return self;
  }
  
  - init  {
    [super init];
    [self init_size: 16 ];
    return self;
  }
  
  + new_size: (int) size {
    return [[self alloc] init_size: size];
  } 
  
  - done {
    if (data_) {
      int index; 
      for (index = 0 ; index < size_ ; index ++) {
        id elem = [self get: index]; 
        [ elem release ];
      } 
      free(data_); 
    }
    return self;
  }
  
  - (BOOL) range_ok: (int) index {
    if (index < 0)      return NO;
    if (index >= size_) return NO;
    return YES;
  }
  
  - get: (int) index {
    if (![self range_ok: index]) return nil;
    return data_[index];
  }
  
  - put_raw: (int) index : (id) value {
    return data_[index] = value;
  }

  
  - put: (int) index : (id) value {
    if (![self range_ok: index]) return nil;
    return [ self put_raw: index : [ value retain ]]; 
  }


@end



