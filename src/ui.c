#include "eruta.h"
#include "image.h"
#include "store.h"
#include "zori.h"
#include "zori_style.h"
#include "zori_screen.h"
#include "zori_console.h"
#include "zori_button.h"
#include "zori_page.h"
#include "zori_menu.h"
#include "zori_longtext.h"
#include "draw.h"
#include "monolog.h"
#include "ui.h"


/* This file contains the concrete implementation of the Eruta GUI
 * and it's various GUI screens and pages. */

static struct ui_state the_ui_state;

char POEM_TEXT[] = 
              "µµµµµ µµµµµµµµµ µµµµµµµ µµµµµµ µµµµµµµµ µµµµ\n"
              "After mankind's summer, winter suddenly came\n"
              "Most reached for the stars, but some remained\n"
              "The earth scarred by the Ancients unhostpitable\n"
              "The hopes low, and so much wisdom forgotten\n"
              "Yet when it seemed the last hour struck\n"
              "Our hope returned, a savior arose\n"
              "Lord Kei, who by Absolute grace\n"
              "Restored the Trees that guard us all from harm\n"
              "Thus ushered in a new millennial spring\n"
              "Lord Kei, watch over us and protect us\n"
              "Until the Travellers return with bounty of the stars\n";

char INTRO_TEXT[] = "Millennia have passed since mankind first traveled to the moon. "
               "Civilizations rose as never before, yet to fall again. " 
               "When all hope seemed lost, the 21 trees sprouted from the earth. " 
               "They brought mysterious powers that protected and strengthened mankind. "
               "Hi!\n\n" 
               "Hello µ world, this is me, 無 life should be\nfun for everyone!";

char INTRO_TEXT2[] = "Something happened, and I was changed!";               

zori_id ui_state_make_sub_menu(zori_id parent, int x, int y, int w, int bh); 


/* Set up HUD. */
void ui_state_init_hud(struct ui_state * ui,
  zori_display * display, zori_font * font) {
    Rebox box;
    
  ui->hud.page = zori_new_page(-1, ui->screen);
  LOG_NOTE("HUD page: %d\n", ui->hud.page);
  box = rebox_make(10, 300, 620, 200);
  ui->hud.dialog = zori_new_longtext(-1, ui->hud.page, &box, POEM_TEXT);
  ui->hud.background_image =  ui->main.background_image;
  /*  store_load_bitmap_id(1004,  "/image/background/bluesky2.jpg"); */
  zori_set_background_bitmap(ui->hud.page, 
                            store_get_bitmap(ui->hud.background_image));  
}

/* Set up Zori GUI. */

void ui_state_init(struct ui_state * ui, 
zori_display * display,
zori_font * font
) { 

  struct zori_style style;
  Rebox box;
  memset(&style, 0, sizeof(style));
  style.text.font  = font;
  style.text.color = color_rgb(255,255,255);
  style.text.flags = ZORI_FONT_ALIGN_CENTRE;
  style.back.color = color_rgba(0, 16, 64, 128);
  
  if ( !ZORI_ID_OK_P(zori_start(&style)) ) {
    LOG_ERROR( "Out of memory when allocating GUI.");
    return;
  }
  
  if (zori_start(&style) != ZORI_ID_OK) {
    LOG_ERROR( "Cannot set up ZORI UI screen.");
    return;
  }
  
  ui->main.background_image = store_load_bitmap_id(1000,  "/image/background/eruta_mainmenu.png");
  ui->mouse_image = store_load_bitmap_id(1001, "/image/gin/fountain-pen_32.png");
  ui->keyjoy_image = store_load_bitmap_id(1002, "/image/gin/fountain-pen_32.png");
  
  
  
  if (ui->main.background_image < 0) {
    LOG_ERROR( "Cannot load main menu background.");
  }
  
  if (ui->mouse_image < 0) {
    LOG_ERROR( "Cannot load mouse cursor image.");
  }
  
  if (ui->keyjoy_image < 0) {
    LOG_ERROR( "Cannot load keyjoy cursor image.");
  }
  
  draw_convert_average_to_alpha(store_get_bitmap(ui->mouse_image), al_map_rgb(255,255,255));
  draw_convert_average_to_alpha(store_get_bitmap(ui->keyjoy_image), al_map_rgb(255,255,255));
  
  ui->screen = zori_new_screen(-1, display);
  
  if (!ZORI_ID_OK_P(ui->screen)) {
    LOG_ERROR( "Cannot set up main screen.");
    return;
  }
  
  
  
  ui->main.page = zori_new_page(-1, ui->screen);
  LOG_NOTE("Main page: %d\n", ui->main.page);
  box = rebox_make(280, 140, 140, 240); 
  ui->main.menu = zori_new_menu(-1, ui->main.page, &box);
  zori_set_background_bitmap(ui->main.page, 
                            store_get_bitmap(ui->main.background_image));
  
  LOG_NOTE("Main menu: %d\n", ui->main.menu);
  
  zori_set_text_font_flags(ui->main.menu, ZORI_FONT_ALIGN_CENTRE);
  
  {
    Rebox box = rebox_make(300, 160, 100, 60);
    ui->main.button.resume = zori_new_button(-1, ui->main.menu, &box, "Continue");
    // zori_set_text_font_flags(ui->main.button.resume, ZORI_FONT_ALIGN_CENTRE);
    LOG_NOTE("Button: %d\n", ui->main.button.resume);
  }
  {
    Rebox box = rebox_make(300, 260, 100, 60);
    ui->main.button.new = zori_new_button(-1, ui->main.menu, &box, "New");
    zori_set_text_font_flags(ui->main.button.new, ZORI_FONT_ALIGN_RIGHT);
    LOG_NOTE("Button: %d\n", ui->main.button.new);
  }
  
  ui_state_init_hud(ui, display, font);
  zori_screen_go(ui->screen, ui->main.page, NULL);
  
}


void ui_handle_main_menu(struct ui_state * ui, int selected) {
  LOG_NOTE("Main menu selected: %d\n", selected);
  if (selected == ui->main.button.resume) {
    LOG_NOTE("Resume");
  } else if (selected == ui->main.button.new) {
    LOG_NOTE("New");
    zori_screen_go(ui->screen, ui->hud.page, NULL);
  }
}

void ui_handle_dialog_text(struct ui_state * ui, int selected) {
  LOG_NOTE("Dialog finished: %d\n", selected);
}


void ui_state_update(struct ui_state * ui, 
  zori_display * display, zori_font * font) {

  int value = 0;
  value = zori_result(ui->main.menu);
  if (value) {
    ui_handle_main_menu(ui, value);
  }
  
  value = zori_result(ui->hud.dialog);  
  if (value) {
    ui_handle_dialog_text(ui, value);
  }
}


#ifdef COMMENT_
/* Direct mode operation? */



  
  INTRO_TEXT2 = 

  def make_sub_menu(parent, x, y, w, h, bh)
    sub_menu = parent.make_menu(x, y, w, h, nil)
    sub_menu.make_button(x, y + 20, w - 20, bh, "Sub choice 1") do
      puts "choice 1"
    end
    sub_menu.make_button(x, y + 30 + bh, w - 20, bh, "Sub choice 2") do
      puts "choice 2"
    end
    sub_menu.make_button(x, y + 40 + 2* bh, w - 20, bh, "Sub choice 3") do
      puts "choice 3"
    end
    sub_menu.fit_children
    sub_menu.hide
    return sub_menu
  end

  def do_main_menu
    main_music    = Music.load(:main, '/music/nethis-the_writer.ogg')
    $lote         = nil
    $lobe         = nil
    if PLAY_MUSIC
      res = main_music.play!
    end
    # res = nil
    # $main_menu = MainMenu.new
    # $main_menu.active = true
    Zori.make_page(:default) do |m|
      State.talk_box            = m.make_longtext(10, 310, 620, 160, "Testing 1 2 3")
      State.talk_box.graph.each { |g| g.font = Eruta::Zori.font.id }
      State.talk_box.delay      = 0.1
      State.talk_box.page_lines = 5
      State.talk_box.page       = 0
      State.talk_box.hide
    end

    
    Zori.make_page(:main_menu) do |m|
      if MAIN_BACKGROUND 
        main_back    = Bitmap.load(:main_back,
                      '/image/background/eruta_mainmenu.png')
        p main_back, main_back.width, main_back.height, main_back.name, main_back.id
                      
        main_back    = Bitmap.load(:main_back,
                      '/image/background/eruta_mainmenu.png')
        p main_back, main_back.width, main_back.height, main_back.name, main_back.id
        p Bitmap[:main_back]
        p Store[:bitmap_main_back]
        m.graph_image(0, 0, main_back.id)
      end

      main_menu     = m.make_menu(250, 190, 120, 440, nil)
      ma            = main_menu

      main_button_1 = ma.make_button(260, 200, 100, 30, "Continue")
      sub_menu = make_sub_menu(main_button_1, 50, 190, 120, 440, 30)
      sub_menu.hide
      
      main_button_2 = ma.make_button(260, 240, 100, 30, "New") do
        do_start_test_map
        Zori.go(:default)
      end
      
      main_button_3 = ma.make_button(260, 280, 100, 30, "Settings") do
        Zori.go(:settings)
      end
      
      main_button_4 = ma.make_button(260, 320, 100, 30, "Instructions")
      main_button_5 = ma.make_button(260, 360, 100, 30, "µ£éè")
      main_button_5 << sub_menu
      
      main_button_5= ma.make_button(260, 400, 100, 30, "Quit") do
        Eruta.quit
      end
      
      main_menu.fit_children
    end

    Zori.make_page(:settings) do |se|
        lote2          = se.make_longtext(100, 10, 160, 100, INTRO_TEXT)
        lote2.delay    = 1
        lote2.page     = 0
        lote2.page_lines = 3
        # $lote2.line_stop = 999990
        
        settings_menu  = se.make_menu(480, 380, 120, 440, nil)
        sm             = settings_menu
        settings_ok_button = sm.make_button(500, 300, 100, 30, "Font 1") do
           if lote2
              lote2.graph.each { |g| g.font = 0 }
           end
        end
        settings_ok_button = sm.make_button(500, 350, 100, 30, "Font 2") do
           if lote2
              lote2.graph.each { |g| g.font = Eruta::Zori.font.id }
           end
           lote2.text = INTRO_TEXT2
        end
        settings_ok_button = sm.make_button(500, 400, 100, 30, "OK") do
           Zori.go(:main_menu)
           if lote
              lote.close
              lote = nil
           end
        end
        sm.fit_children
    end

    Zori[:main_menu].hide
    Zori[:settings].hide

    Zori.go(:main_menu)
  end
#endif


