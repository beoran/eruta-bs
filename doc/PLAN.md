
= ERUTA BLESSED SAVIOR PLAN =
 
== General ==

Eruta Blesed savior is a refocus of several previous idead. Basically it is 
based on Eruta's game engine, but the plot will bet hat of Eruta Kei's Quest.
I decided this because the EKQ plot I have in mind is to e more interesting than 
the one I imagined for Eruta. Furthermore the dungeon crawling 3d engine I was
working on for EKQ had several problems, the most important one that it leads to
what I feel is boring gameplay.

 

Eruta will be a 2D game with a classic tile map and a top-down view. 
  Rationale: I like the old-school 2D look. 3D games are more resource hungry 
  and time-consuming to implement, so that's for the next project.

Programmed in C with mruby scripting:
  Rationale: I love Ruby, but no matter how easy normal Ruby is to use, 
  I ended up writing a lot of C anyway to support it. For gaming, speed is
  essential since a slow game is hard to enjoy, especially for an action game.
  So I decided to rather than use ruby extended with C, to use C
  extended with a scripting language.
  At first the choice of scripting languages was on Lua, but the mruby idea 
  and open source development on github came through, and I switched to mruby 
  since it does give me the sweet, easy Ruby syntax for the scripts. :)
  And no, don't even dare mention C++, it complicates matters without being 
  actually that helpful. And too bad that Objective C is not well supported on 
  Windows and messy on Linux.
  
Low level and libraries in C, game script and configuration in mruby.
  Change Eruta a to keep the scripting language for scripting, not for 
  implementing the engine itself anymore.    
  One problem with the Eruta engine is that the functionality isn't divided well.
  Arguably, too much ow level stuff is implemented in Ruby, which leads to
  the interface between C and mruby being huge (too many links). 
  For example, there is a 2D scene engine in C that is only used to implement 
  he GUI in mruby. 
  Furthermore since C compiles pretty fast for me now, there isn't any 
  compelling reason anymore to use scripting for "rapid" development. I have 
  started to C's.appreciatethat fact that the C compiler does warn me of some silly 
  errors that I would only detect on run time on the mruby side. 
  The EBS engine will push the GUI, the game control and all of the physics back 
  to C, and limit the interface with mruby to what is needed for scripting.
  
Using Allegro 5.x.
  Rationale: SDL may be the de facto standard for open source games, but it's
  very poor in functionality. So I ended up having to write a lot of
  functionality myself. And SDL 2.0 is not as mature as allegro 5 is. I thought
  Allegro 5 was slower, but when using plain C, and taking care to
  load the images after opening the screen, Allegro 5 was actually plenty
  fast enough. Allegro has so much nice functionality built in, it would be a
  waste not to use it when using plain C. Any speed problems with Allegro 5
  can be dealt with by sing the primitives add on with it's indexed pixel
  buffer rendering.
  
Virtual screen resolution exactly 640x480. Color depth minimum 16bpp or better.
  Also implement scaling to other resolutions trough OpenGL or 2xSai, etc 
  to achieve resolution dependence later.
  Rationale: Old-style feeling and limit art requrements.

Hand drawn style art with colors limited to a 12/15 bits palette. 
  I like bright colors that have a simple rgb expression and generated
  a palette that fits the limitations for drawing the hand drawn art. 
  Art based on SVG, or even generated from 3D models is OK as well as log as 
  it acieved the desired look.  Rationale: Old-style feeling.

A role playing game with an interesting story.
  Rationale: That's what RPG's are about.

A role playing game with much onus on exploration.
  Rationale: Some famous games, like, say FFXII forget this, but one of the main 
  attractions of RPG's is exploration. For EBS I want to achieve this 
  by procedurally generating a huge world from the world map. It will not be 
  random, all players will play the same world, but the procedural generation 
  will be used to ease game creation. The generated world will be hand-edited 
  where necessary. It will be split up in to tile maps, one map for every tile 
  on the world map. 
  
Multi player game:
  Rationale: I want to be able to play together with someone on the same PC. 
  To begin just 2 players together is OK. Perhaps implement 
  split screen. 2 to 4 players are probably OK to divide the screen between. The 
  maximum should be the amount of 12 characters, but it will be hard to fit all 
  those on one screen. So perhaps allow networked play as well.

Fewer artificial limits:
  One thing that plagues RPG's are arbitrary limits such as the amount of 
  characters in your active party. I'd like to have less of such limits if 
  possible. All playable characters are part of your party and they are all 
  AI controlled  except the player's selected character. The player can set up 
  various strategies and influence the actions of the other in party PC's. 
  No unreasonable time limits, but also, no limits on the amounts of NPC's or 
  foes.
  
  
Hybrid Action RPG
  A live action RPG with a cooldown system like that of SOM. You can attack 
  wildly but calm attacks are much more effective. Also find a way to allow for 
  combos  and weapon techniques. The non player contolled PC's will fight much 
  like in FF12, in a programmabe/commandable way. Also allow for a "RPG" 
  mode later on where the attack button works like in SD3, that leads to an 
  almost MMORPG style of control. Items and spells may be menu based but 
  prevent spell spamming like in SOM by also adding a cooldown.  
  The players will be able  to use various forceful actions outside of combat 
  as well.
  
  
== Needed Functionality ==

The Eruta game engine will need the following general functionality: 

Tile map 
Sprites
Physics, collision detection and resolution
Particle engine for effects
Camera module
Resource loading
UI Subsystem
Input mapping (to commands/events)
Music handling
Sound handling
Scripting of maps and events.
Game system: stats, skills, techniques, battle resolution, etc, etc.


=== Tile Map Requirements ===

Tile map with tiles of 32x32 pixels. Before, 40x40 or 64x64 were considered, but
these are too large and make the art requirements too high. Also, 32x32 is easy 
to accelerate with OpenGL later, and fits the resolution of 640x480 well.

Tile map distinguishes between tile layers wich dictate rendering and world
layers which dictate gameplay (ie, on which levels the sprites can “be”).
Tile map has at least 4 drawing layers of tiles. Testing shows 4 is optimal 
for speed. However, to allow for "inside" locations to be mapped easily to 
the tile map, there may be 4 additional inside layers that are shown if the 
global game state is "inside". Probably it's easiest to have an arbitrary
amount of layers, each with a inside/outside flag. Additionally, layer offsets 
could be used to "compress" inside locations and elevation and prevent 
tile layers that are mostly empty.   

Layers are usually grouped in pairs for the “world layers”. Layer 1 is the first
floor, layer 2 is anything that stands on that floor and any fringes/overlaps
from one kind of floor to another. Layers 1 and 2 are always under all sprites.
Layer 3 and 4 are normally the “floor above” the player, however, the player may
walk there in case of bridges, overlays, or parallax. Again, to enhance this, 
the role of the layers could be arranged by layer flags.

Every layer may have a background image that is drawn before the tiles of the 
layer  are drawn. The tiles of a layer can also be made invisible if the 
background layer by itself is enough visually. In that case the tiles influence 
the game play and physics only. Perhaps like tmx it would actually be simpler 
to allow arbitrary background images with and without an offset and with or 
without parallax.

Although only one bitmap background can be displayed per layer, it's possible 
for a layer to change the background depending on what's needed. 
(night/day/scripting/etc). The background bitmap width and 
height must have a size that is an integer multiple of 32 pixels.

There will be options for parallax scrolling of the background bitmap versus the 
tiles in a layer, or, for paralax between the layers in total. Scrolling
can be locked or unlocked in x and y directions though scripts or by using
special "scroll lock" tiles.

View culling will be possible, in that, if enabled, only the direct area around the 
player sprite will be shown, and anything outside a rectangle set by special 
culling tiles will not be drawn. This too allows inside areas to be stored more 
compactly. Although te solution of "inside layers" may be actually simpler
and preferable.

Every map layer uses exactly one tile set based on a single tile image. Tile 
images  are are files in .png format with a width and height that are integer 
multiples of 32, and that contain many individual tiles. The current term 
would be "atlas textures".

Keeping with convention over configuration, the tile sets are read in, split up
in 32x32 tiles and interpreted according to the following rules:

Every map describes the names of the tile it uses. The file name of the 
set is of the form set_name_xxx000.png where xxx000 is a number with at least 4 
digits that signifies the start index of the set. Tiles in a set are numbered 
from left to right and from top to bottom, starting from 0. So, for instance  
the second tile at  the top left of a tile set will be numbered 1. Alternatively 
the tmx tile offset is used.

The idea of this is for maps broadly compatible with each other even if 
they use different tile sets, and also to make it easy to swap tile sets if 
needed.

The map engine loads tiled .tmx map files. The properties of a tile, such as 
solidity or animation are loaded from the Tiled file. Tiled has animated tiles 
since tiled 0.10 so support those as well.

The tile engine also supports automatic tile blending and automatic shadows
to lower the art requirements.

Maybe rewrite to use indexed primitives for performance, although it does mean
that atlases will need to be generated to allow automatic blends to work.


Particle and effect engine
--------------------------

This module can display effects like rain, snow, blood, bursts. Explosions 
and spell effects would be nice as well. Will use the camera for correct views. 
allow bitmaps as well as drawing for the particles. Need to allow bitmap
animations for certain effects. Probably best to use indexed primitives 
for performance. 


Camera module
-------------
The camera module should model a camera, that is, a view of a
rectangular area that can move in the “world” of the game and translate these
world coordinates to screen coordinates. 


Physics
-------
Finally got rid of Chipmunk. I was able to implement a useful system 
that's simpler than chipmunk but that works well and doesn't require bazillions
of static shapes for the tile map walls. Also need to implement battle related 
area sensitivity and combat mechanics in C. Trying to split this between
mruby and C was a bit of a disaster.

UI
--
Implement Zori gui in C. I considered an "immediate mode" gui, but that seems 
to be hard to script. So go with a semi-deferred GUI for now.


Game Play
---------

Based on the system I considered for WOE. Skill based, many different 
proefficiencies. Level based on skills. Perhaps have a generic 
"experience" jar as well? Then again games from Bethesda don't seem to need 
general experience, and those are also all RPG.


+---------+
| Modules |
+---------+

According to the requirements, the following modules will be needed, each
in one C file (although smaller modules may be joined tohgether into one c
file if appropriate)

camera.c  : Camera, active view of current area.
bevec.c   : 2D vectors and points.
bump.c    : Physics and collision detection.
dynar.c   : Dynamic arrays.
alps.c    : Graphical effects using particle engine and likewise.
every.c   : Iterator for dunamic arrays, etc.
toruby.c  : Functions that will be used in the ruby scripts
glh.c     : OpenGL helper functionality.
rh.c      : Ruby helpers.
main.c    : Main function of program.
mem.c     : Memory management and malloc wrappers.
mobile.c  : Mobile entities 
sound.c   : Sound and music helpers.
state.c   : Program state, and also, high level functionality.
tile.c    : Tile functionality.
tilemap.c : Tile map functionality.
tilepane.c: Tile Layer functionality.
sprite.c  : Sprites on the tile map.
mode.c    : Different game modes (menu, normal, ect)
menumode.c: Menu mode
playmode.c: Play mode
openmode.c: Opening (intro) mode
confmode.c: Configuration/settings mode.
editmode.c: Edit mode for built in editor.
goal.c    : Goal of motion of mobile or camera.
player.c  : Player control and data.
ncp.c     : Npc control and data.
foe.c     : Foe control and data.
switch.c  : Switches.
treasure.c: Treasures.
door.c    : Doors.
obstacle.c: Obstacles, pushblocks, etc.
route.c   : Path finding and planning.
item.c    : Items player can have.
gear.c    : Equippable items.
useable.c : Useable items.
being.c   : Logical and play-related information about beings.
inmap.c   : Input mapping (links input to in-game action).
ai.c      : AI for npc's and foes.
saveload.c: Saving and loading state.
initfile.c: Loading and saving settings.
react.c   : Map events and NPC/Item/Foe reactions.
world.c   : World state.
ui.c      : UI.
widget.c  : UI widget.
wimenu.c  : UI menu widget.    
wibox.c   : UI text box widget (more widgets needed).

(Probably more...)

Maybe     : 
intmap.c  : Maps game objects to integers and vice versa for easy lua wrapping?

47 modules/c files -> upper estimate 80 modules.
80 * 1000lines per file (upper limit) = 80kloc.
